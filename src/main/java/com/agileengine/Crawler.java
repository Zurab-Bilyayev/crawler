package com.agileengine;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/***
 * Agileengine test task
 */
public class Crawler {

    private final static Logger logger = LoggerFactory.getLogger(Crawler.class);

    private final static String IGNORE_TAG_NAME = "div";
    private final File sourceFile;
    private final File targetFile;
    private String id = "make-everything-ok-button";

    public Crawler(final File source, final File target) {
        this.sourceFile = source;
        this.targetFile = target;
    }

    public void setTargetId(final String id) {
        this.id = id;
    }

    /**
     * Get attributes of the element (and include own text as attribute)
     * @param element
     * @param isLog
     * @return
     */
    private Attributes getAttributes(final Element element, boolean isLog) {
        element.attributes().put("content", element.ownText());
        if (isLog) {
            logger.info("Source element: {}", element);
            for (Attribute attribute : element.attributes()) {
                logger.info(" {}: {}", attribute.getKey(), attribute.getValue());
            }
        }
        return element.attributes();
    }

    /**
     * Get css selector for parent of element
     * @param element
     * @return
     */
    private String getParentCssSelector(final Element element) {
        String parentCssSelector = element.parent().cssSelector();
        logger.info("Parent css selector: {}", parentCssSelector);
        return parentCssSelector;
    }

    /**
     * Find correct element base of template
     * @return css path to element
     */
    public String findElement() {
        Optional<Element> elementById = JsoupFindByIdSnippet.findElementById(sourceFile, id);
        if (elementById.isPresent()) {
            Element sourceElement = elementById.get();
            Attributes sourceAttributes = getAttributes(sourceElement, true);

            Optional<Elements> targetParentElements = JsoupCssSelectSnippet.findElementsByQuery(targetFile, getParentCssSelector(sourceElement));
            if (targetParentElements.isPresent()) {
                Elements allElements = removeDivElements(targetParentElements.get());
                int size = allElements.size();
                if (size == 0) {
                    logger.info("Document is not correct. No elements.");
                } else if (size == 1) {
                    logger.info("Found 1 element");
                    Map<Element, List<Attribute>> compareByValue = new HashMap<>();
                    Element element = allElements.get(0);
                    compareAttributesByValue(element, sourceAttributes, compareByValue);
                    printMatchingAttributes(compareByValue.entrySet().iterator().next());
                    return element.cssSelector();
                } else {
                    logger.info("Found {} elements", size);
                    Map<Element, List<Attribute>> compareByValue = new HashMap<>();
                    allElements.forEach(el -> compareAttributesByValue(el, sourceAttributes, compareByValue));
                    return getMostSuitableElement(compareByValue).cssSelector();
                }
            }
        }
        return "";
    }

    /**
     * Remove elements with tag name 'div' from elements
     * @param elements
     * @return
     */
    private Elements removeDivElements(final Elements elements) {
        Elements filterElements = new Elements();
        elements.forEach(element -> element.getAllElements()
                .stream()
                .filter(child -> !child.tagName().equalsIgnoreCase(IGNORE_TAG_NAME))
                .forEach(filterElements::add));
        return filterElements;
    }

    /**
     * Print attributes
     * @param entry
     */
    private void printMatchingAttributes(final Map.Entry<Element, List<Attribute>> entry) {
        logger.info("Found element: " + entry.getKey().cssSelector());
        entry.getValue().forEach(attr -> logger.info(" {}: {}", attr.getKey(), attr.getValue()));
    }

    /**
     * Find most suitable element
     * @param map
     * @return element
     */
    private Element getMostSuitableElement(final Map<Element, List<Attribute>> map) {
        int size = -1;
        Map.Entry<Element, List<Attribute>> entry = null;
        for (Map.Entry<Element, List<Attribute>> elementListEntry : map.entrySet()) {
            List<Attribute> value = elementListEntry.getValue();
            if (value.size() > size) {
                size = value.size();
                entry = elementListEntry;
            }
        }
        printMatchingAttributes(entry);
        return entry.getKey();
    }

    /**
     * Cjech if attribute include to the attributes by key
     * @param attribute
     * @param attributes
     * @return
     */
    private boolean isAttribute(final Attribute attribute, final Attributes attributes) {
        return attributes.asList().stream().anyMatch(attr -> attr.getKey().equals(attribute.getKey()));
    }

    /**
     * Compare attributes by value and collect compared attributes to map
     * @param element
     * @param attributes
     * @param compareByValue
     */
    private void compareAttributesByValue(final Element element, final Attributes attributes, final Map<Element, List<Attribute>> compareByValue) {
        Attributes targetAttributes = getAttributes(element, false);
        List<Attribute> compareValues = targetAttributes.asList().stream()
                .filter(attr -> isAttribute(attr, attributes))
                .filter(attr -> attributes.get(attr.getKey()).equals(attr.getValue()))
                .collect(Collectors.toList());
        if (compareValues.size() > 0) {
            compareByValue.put(element, compareValues);
        }
    }

    /**
     * Start point of the app
     * arg0 - absolute path to the source file
     * arg1 - absolute path to the target file
     * arg2 - id for looking for (by default "make-everything-ok-button")
     * @param args
     */
    public static void main(String[] args) {
        String sourceFile = "";
        String targetFile = "";
        String id = "";
        if (args.length > 1) {
            sourceFile = getArgument(args[0]);
            targetFile = getArgument(args[1]);

            if (args.length == 3) {
                id = getArgument(args[2]);
            }

            File source = new File(sourceFile);
            File target = new File(targetFile);
            if (source.exists() && target.exists()) {
                Crawler crawler = new Crawler(source, target);
                if (!"".equals(id)) {
                    crawler.setTargetId(id);
                }
                String cssPath = crawler.findElement();
                System.out.println("Element is found: " + cssPath);
            } else {
                System.out.println("Source '" + sourceFile + "' or Target '" + targetFile + "' are not found");
            }
        } else {
            printHelp();
        }
    }

    private static void printHelp() {
        System.out.println("Crawler '<absolute path to the source file>' 'absolute path to the target file>' '<id for search>'");
    }

    private static String getArgument(final String argument) {
        return argument.subSequence(1, argument.length() - 1).toString();
    }
}
